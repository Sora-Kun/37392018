# FIRST Robotics Team 3739: Oakbotics

[![Competition Ready](https://img.shields.io/badge/competition%20ready-no-red.svg)]() | 
[![Drive](https://img.shields.io/badge/drive-no-red.svg)]()
[![Autonomous](https://img.shields.io/badge/autonomous-no-red.svg)]() 
[![Vision](https://img.shields.io/badge/vision-no-red.svg)]()

## About Us

Welcome to the Git repository for FIRST Robotics team 3739: Oakbotics.
Here you will find the code currently being written by the programming team 
for the FRC 2018 season! We are currently using Java.

## Repository Structure ⚠

DEVELOPERS, THIS IS IMPORTANT!

 - This repository is going to be structed in terms of features. 
 - For each feature that is added, a new branch of master will be created
 - Specifically to house very preliminary testing code is the "prototyping" branch
 - The prototyping branch will contain an Eclipse project for each feature that is being prototyped
 - The prototyping branch will never be merged with master

### Prototyping Branch 

This branch will be treated as a sort of sandbox to consolidate all features in preliminary testing, 
before those features get their own branches. The reason for this is so that 
explicity prototype code is seperated from feature code that is destined to be 
mereged with master. 